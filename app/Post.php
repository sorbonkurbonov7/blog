<?php

namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use Sluggable;

    protected $fillable = ['title', 'content', 'date'] ;
    const IS_PUBLIC = 1;
    const IS_DRAFT = 0;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(
          Tag::class,
          'post_tags',
            'post_id',
            'tag_id'
        );
    }


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function add($fields)
    {
        $post = new static();
        $post->fill($fields);
        $post->user_id = 1;
        $post->save();

        return $post;
    }

    public function edit($fielda)
    {
        $this->fill($fielda);
        $this->save();
    }

    public function remove()
    {
        $this->removeImage();
        $this->delete();
    }

    public function removeImage()
    {
        if ($this->image != null)
        {
            Storage::delete('uploads/' . $this->image);
        }

    }

    public function uploadImage($image)
    {
        if ($image == null) {return;}
        $this->removeImage();
        $filename = str_random(10). '.' .$image->extension();
        $image->storeAs('uploads/', $filename);
        $this->image = $filename;
        $this->save();
    }

    public function getImage()
    {
        if ($this->image == null)
        {
            return '/img/user/no-image.png';
        }
        return '/uploads/'. $this->image;
    }

    public function setCategory($id)
    {
        if ($id == null){return;}
        $this->category_id = $id;
        $this->save();
    }

    public function setTags($ids)
    {
        if ($ids == null ) {return;}

        $this->tags()->sync($ids);
        $this->save();
    }

    public function setDraft()
    {
        $this->status = Post::IS_DRAFT;
        $this->save();
    }

    public function setPublic()
    {
        $this->status = Post::IS_PUBLIC;
        $this->save();
    }

    public function toggleStatus($value)
    {
        if ($value == null)
        {
            return $this->setDraft();
        }
        return $this->setPublic();
    }


    public function setFeatyred()
    {
        $this->is_featured = 1;
        $this->save();
    }

    public function setStandart()
    {
        $this->status = 0;
        $this->save();
    }

    public function toggleFeatured($value)
    {
        if ($value == null){
            return $this->setStandart();
        }
        return $this->setFeatyred();
    }

    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('d/m/y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }

    public function getDateAttribute($value)
    {
        $date = Carbon::createFromFormat('Y-m-d', $value)->format('d/m/y');
        return $date;
    }

    public function getCategoryTitle()
    {
        return ($this->category != null)
            ? $this->category->title
            : 'Нет категории';
    }

    public function getCategoryTags()
    {
        return (!$this->tags->isEmpty())
            ? implode(', ', $this->tags->pluck('title')->all())
            : 'Нет тегов';
    }
































}
