<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public function add($email)
    {
        $sub = new static;
        $sub->email = $email;
        $sub->token = str_random(50);
        $sub->save();

        return $sub;
    }

    public function remove()
    {
        $this->delete();
    }
}
